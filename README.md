# RPi-Hawkbit-Client

## Targets

All Raspberry Pis

1. with latest Raspbian
2. with a Yocto Linux (later)

Most industrial Yocto Linux devices (later)

## References

* rauc-hawkbit-client:
   * https://github.com/rauc/rauc-hawkbit#rauc-hawkbit-client 
   * https://www.youtube.com/watch?v=SLX0Fd_y6Cc


## Hawkbit

Allow the Target to be updated by a Hawkbit Backend

* https://www.kynetics.com/iot-platform-update-factory
* https://www.bosch-iot-suite.com/
* https://medium.com/@chopra.raghav2180/iot-part1-using-hawkbit-to-update-softwares-in-raspberry-pis-475717d1bf5f



