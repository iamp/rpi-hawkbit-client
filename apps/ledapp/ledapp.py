#! /usr/bin/env python3

import RPi.GPIO as GPIO
import time

switch_pin = 16
led_red_pin = 21


GPIO.setmode(GPIO.BCM)

# the LED
GPIO.setup(led_red_pin, GPIO.OUT)

# the switch
#GPIO.setup(switch_pin, GPIO.IN)
GPIO.setup(switch_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

for i in range(5):
    GPIO.output(led_red_pin, GPIO.HIGH)
    print('HIGH')
    time.sleep(0.5)
    GPIO.output(led_red_pin, GPIO.LOW)
    print('LOW')
    time.sleep(0.5)

try:
    # Loop
    while True:
        if GPIO.input(switch_pin) == 1:
            # On
            GPIO.output(led_red_pin, GPIO.LOW)
            #print('LOW', GPIO.input(switch_pin))

        else:
            # Off
            GPIO.output(led_red_pin, GPIO.HIGH)
            #print('HIGH')


except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
    GPIO.cleanup() # cleanup all GPIO