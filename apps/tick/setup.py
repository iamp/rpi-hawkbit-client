#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
      name='tick',
      description='minimal asyncio example',
      author='Eugene Nuribekov',
      author_email='enuribekov@gmail.com',
      license='LGPL-2.1',
      use_scm_version=False,
      url='',
      setup_requires=['setuptools_scm'],
      install_requires=[
          'asyncio',
      ],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      scripts=[
          'tick.py'
      ]
)
