# HBLoader

Intended to install software from Eclipse HawkBit server.

Software based on rauc-hawkbit

## Prerequisites

Hardware: Raspberry PI

OS: Raspbian-buster

## Install

`python3 setup sdist`

`pip3 install --user dist/hbloader-x.x.x.tar.gz`

`export PATH=$PATH:$HOME/.local/bin`

If you need to use RPi.GPIO:

`sudo usermod -aG gpio $USER`

## Configure
hboader

On first run programm asks following parameters:

* HawkBit IP address
* HawkBit TCP port (default: 8080)
* Device name (visible name)
* Management interface login (default: admin)
* Management interface password (default: admin)
* Run as service' - run installed software as Linux service
