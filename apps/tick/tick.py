#! /usr/bin/env python3

import asyncio

async def main():
    while True:
        print('tick')
        await asyncio.sleep(3)

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
