#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
      name='hbloader',
      version='1.0.0',
      description='hawkBit client',
      author='Eugene Nuribekov',
      author_email='enuribekov@gmail.com',
      license='LGPL-2.1',
      setup_requires=['wheel'],
      install_requires=[
          'aiohttp>=3.3.2',
      ],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      scripts=[
          'bin/hbloader.py'
      ]
)
