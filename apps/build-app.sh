#! /usr/bin/env bash

dirname=$1
imgname=debian-$1

time docker image build --no-cache --build-arg app=$1 -t $imgname -f Dockerfile-app .

#time docker image save -o $dirname/$imgname.tar $imgname 

#time gzip $imgname.tar
