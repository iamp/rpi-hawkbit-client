#! /usr/bin/env python3

import asyncio

async def main():
    while True:
        print('tock')
        await asyncio.sleep(1)

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
